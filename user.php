<?php
require_once 'utils/bootstrap.php';

if(!isUserLoggedIn()){
	header('Location: /login.php');
	exit();
}

if(isset($_GET["id"])){
	$user = $_GET["id"];
}else{
	$user = getUsername();
}

$templateParams["user"] = $dbh->getUser($user);

require 'template/user.php';
?>