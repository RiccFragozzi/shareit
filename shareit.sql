-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2023 at 10:34 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shareit`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `content` varchar(256) NOT NULL,
  `user_id` varchar(16) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `content`, `user_id`, `post_id`) VALUES
(37, 'Complimenti!', 'checcozalone', 24),
(38, 'Capelli stupendi', 'fedez', 21),
(39, 'Il mio film preferito!!', 'chiaraferragni', 27),
(40, 'Vero, stupenda', 'checcozalone', 17);

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE `friends` (
  `user1_id` varchar(16) NOT NULL,
  `user2_id` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `friends`
--

INSERT INTO `friends` (`user1_id`, `user2_id`) VALUES
('checcozalone', 'chiaraferragni'),
('chiaraferragni', 'checcozalone'),
('chiaraferragni', 'fedez'),
('fedez', 'checcozalone'),
('fedez', 'chiaraferragni');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `post_id` int(11) NOT NULL,
  `user_id` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`post_id`, `user_id`) VALUES
(24, 'checcozalone'),
(24, 'fedez'),
(23, 'fedez'),
(21, 'fedez'),
(28, 'chiaraferragni'),
(20, 'checcozalone'),
(29, 'fedez');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `href` varchar(256) NOT NULL,
  `source_user` varchar(16) NOT NULL,
  `user_id` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `href`, `source_user`, `user_id`) VALUES
(37, 1, '/user.php?id=checcozalone', 'checcozalone', 'chiaraferragni'),
(39, 2, '/content.php?id=24', 'checcozalone', 'chiaraferragni'),
(40, 3, '/content.php?id=24', 'checcozalone', 'chiaraferragni'),
(41, 1, '/user.php?id=fedez', 'fedez', 'checcozalone'),
(42, 1, '/user.php?id=fedez', 'fedez', 'chiaraferragni'),
(43, 2, '/content.php?id=24', 'fedez', 'chiaraferragni'),
(44, 2, '/content.php?id=23', 'fedez', 'chiaraferragni'),
(45, 3, '/content.php?id=21', 'fedez', 'chiaraferragni'),
(46, 2, '/content.php?id=21', 'fedez', 'chiaraferragni'),
(47, 1, '/user.php?id=chiaraferragni', 'chiaraferragni', 'fedez'),
(48, 1, '/user.php?id=chiaraferragni', 'chiaraferragni', 'checcozalone'),
(49, 2, '/content.php?id=28', 'chiaraferragni', 'checcozalone'),
(50, 3, '/content.php?id=27', 'chiaraferragni', 'checcozalone'),
(51, 2, '/content.php?id=20', 'checcozalone', 'fedez'),
(52, 3, '/content.php?id=17', 'checcozalone', 'fedez'),
(53, 3, '/content.php?id=28', 'fedez', 'checcozalone');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `media` varchar(512) NOT NULL,
  `description` varchar(256) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `user_id` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `media`, `description`, `timestamp`, `user_id`) VALUES
(16, '/uploads/1689632714.png', 'Sono pensieroso', 1689632723, 'fedez'),
(17, '/uploads/1689632746.png', 'Che bella Roma!!', 1689632757, 'fedez'),
(18, '', 'XFactor, grazie a tutti voi', 1689632781, 'fedez'),
(19, '/uploads/1689632788.png', 'Finalmente il battesimo .D', 1689632801, 'fedez'),
(20, '/uploads/1689632815.png', 'La mia famiglia', 1689632822, 'fedez'),
(21, '/uploads/1689633106.png', '', 1689633107, 'chiaraferragni'),
(22, '/uploads/1689633114.png', '', 1689633117, 'chiaraferragni'),
(23, '/uploads/1689633123.png', 'Ho scoperto che Fedex è un ottimo fotografo', 1689633166, 'chiaraferragni'),
(24, '/uploads/1689633186.png', 'Oggi battesimo di Leo :D :D', 1689633205, 'chiaraferragni'),
(25, '/uploads/1689633585.png', 'Che caldo che fa qui', 1689633592, 'checcozalone'),
(26, '/uploads/1689633610.png', '', 1689633613, 'checcozalone'),
(27, '/uploads/1689633620.png', 'Il miglior cast di tutti i tempi', 1689633632, 'checcozalone'),
(28, '/uploads/1689633636.png', 'Grazie a tutti voi', 1689633645, 'checcozalone'),
(29, '/uploads/1689712095.png', 'ciao a tutti', 1689712097, 'fedez');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(16) NOT NULL,
  `avatar` varchar(512) NOT NULL,
  `email` varchar(128) NOT NULL,
  `name` varchar(64) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `avatar`, `email`, `name`, `surname`, `password`) VALUES
('checcozalone', 'uploads/checcozalone.png', 'checcozalone@gmail.com', 'Checco', 'Zalone', '4cc059458d5d83b168cb9bb1d338e15e'),
('chiaraferragni', 'uploads/chiaraferragni.png', 'chiaraferragni@gmail.com', 'Chiara', 'Ferragni', '243a3b6f7ddfea2599743ce3370d5229'),
('fedez', 'uploads/fedez.png', 'fedez@gmail.com', 'Fedez', '', 'a401db31d058482079a5f6ab23287675');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_post_id` (`post_id`),
  ADD KEY `comments_user_id` (`user_id`);

--
-- Indexes for table `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`user1_id`,`user2_id`),
  ADD KEY `afriends_user2_id` (`user2_id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD KEY `likes_post_id` (`post_id`),
  ADD KEY `likes_user_id` (`user_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_user_id` (`user_id`),
  ADD KEY `notification_source_user_id` (`source_user`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `friends`
--
ALTER TABLE `friends`
  ADD CONSTRAINT `afriends_user2_id` FOREIGN KEY (`user2_id`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `friends_user1_id` FOREIGN KEY (`user1_id`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `likes`
--
ALTER TABLE `likes`
  ADD CONSTRAINT `likes_post_id` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `likes_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notification_source_user_id` FOREIGN KEY (`source_user`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notifications_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
