function reload(){
	location.reload();
}

function logout(){
	location.href = "logout.php";
}

function home(){
	location.href = "/";
}

function profile(){
	location.href = "/user.php";
}

function createPost(description, media){	
	$.post(
		"api/post-create.php",
		{ description: description, media: media },
		function(data) {
			data = JSON.parse(data);
			if(data.code != 1){
				alert("Si è verificato un errore.");
				return;
			}
			profile();
		}
	);
}

function likePost(postID){
	var liked = $("#like-" + postID).attr("val") == "1";
	
	$.post(
		"api/post-like.php",
		{ post_id: postID, like_action: liked ? "0" : "1" },
		function(data) {
			data = JSON.parse(data);
			if(data.code != 1){
				alert("Si è verificato un errore.");
				return;
			}
			
			$("#like-" + postID).attr("val", (liked ? "0" : "1"));
			$("#like-" + postID).attr("src", (liked ? "css/heart-regular.svg" : "css/heart-solid.svg"));
		}
	);
}

function commentPost(postID, content){
	$.post(
		"api/post-comment.php",
		{ post_id: postID, content: content },
		function(data) {
			data = JSON.parse(data);
			if(data.code != 1){
				alert("Si è verificato un errore.");
				return;
			}
			
			$("#comment-" + postID).val("");
			reload();
		}
	);
}

function followUser(userID){
	var followed = $("#follow-" + userID).attr("val") == "1";
	
	$.post(
		"api/user-follow.php",
		{ user_id: userID, follow_action: followed ? "0" : "1" },
		function(data) {
			data = JSON.parse(data);
			if(data.code != 1){
				alert("Si è verificato un errore.");
				return;
			}
			
			$("#follow-" + userID).attr("val", (followed ? "0" : "1"));
			$("#follow-" + userID).html(followed ? "Segui" : "Non seguire");
			reload();
		}
	);	
}

function usersSearch(){
	var query = $("#users-input").val();
	
	if(query.length == 0){
		$("#users-result").html("");
		return;
	}
	
	$.post(
		"api/user-search.php",
		{ query: query },
		function(data) {
			data = JSON.parse(data);
			if(data.code != 1){
				alert("Si è verificato un errore.");
				return;
			}
			var result_html = "";
			$("#users-result").html();
			data.output.forEach(function(user) {
				var user_avatar = user.avatar;
				var user_name = user.name + " " + user.surname;
				var user_id = user.username;
				result_html += 
				`<div class="mt-2">
					<img class="rounded-circle" width="20" height="20" src="${user_avatar}" alt="avatar di ${user_name}">
					<a href="user.php?id=${user_id}">${user_name}</a>
				</div>`;
			});
			$("#users-result").html(result_html);
		}
	);	
}

function commentConfirm(postID){
	var content = $("#comment-" + postID).val();
	commentPost(postID, content);
}