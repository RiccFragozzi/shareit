<?php
	require_once 'api.php';

	if(!isset($_POST["post_id"]) || !isset($_POST["content"]) || strlen($_POST["content"]==0)){
		$result["code"] = "-1";
		$result["error"] = "missing post_id or content parameter";
		die(json_encode($result));
	}
	
	$result["code"] = $dbh->commentPost($username, $_POST["post_id"], $_POST["content"]);
	
	echo json_encode($result);
?>