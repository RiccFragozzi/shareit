<?php
	require_once 'api.php';

	if(!isset($_POST["user_id"]) || !isset($_POST["follow_action"])){
		$result["code"] = "-1";
		$result["error"] = "missing user_id or follow_action parameter";
		die(json_encode($result));
	}
	
	if($_POST["follow_action"] == 0){
		$result["code"] = $dbh->unfollowUser($username, $_POST["user_id"]) ? "1" : "-1";
	}
	
	if($_POST["follow_action"] == 1){
		$result["code"] = $dbh->followUser($username, $_POST["user_id"]) ? "1" : "-1";
	}
	
	echo json_encode($result);
?>