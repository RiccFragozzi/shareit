<?php
	require_once 'api.php';

	if(!isset($_POST["description"]) || !isset($_POST["media"])){
		$result["code"] = "-1";
		$result["error"] = "missing description or media parameter";
		die(json_encode($result));
	}
	
	$result["code"] = $dbh->createPost($username, $_POST["description"], $_POST["media"]) ? "1" : "-1";
	
	echo json_encode($result);
?>