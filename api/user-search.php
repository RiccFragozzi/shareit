<?php
	require_once 'api.php';

	if(!isset($_POST["query"])){
		$result["code"] = "-1";
		$result["error"] = "missing query parameter";
		die(json_encode($result));
	}
	
	$result["code"] = "1";
	$result["output"] = $dbh->searchUser($_POST["query"]);
	
	echo json_encode($result);
?>