<?php
	require_once 'api.php';

	if(!isset($_POST["post_id"]) || !isset($_POST["like_action"])){
		$result["code"] = "-1";
		$result["error"] = "missing post_id or like_action parameter";
		die(json_encode($result));
	}
	
	if($_POST["like_action"] == 0){
		$result["code"] = $dbh->dislikePost($username, $_POST["post_id"]) ? "1" : "-1";
	}
	
	if($_POST["like_action"] == 1){
		$result["code"] = $dbh->likePost($username, $_POST["post_id"]) ? "1" : "-1";
	}
	
	echo json_encode($result);
?>