<?php
	session_start();
	
	//RETURN CODE:
	// 1	success
	// -1	execution error
	// -2	missing parameter
	// -3	not logged in
	
	$result = array();

	if(!isset($_SESSION["username"]) || strlen($_SESSION["username"]) == 0){
		$result["code"] = "-3";
		$result["error"] = "user not logged in";
		die(json_encode($result));
	}
	
	require_once("../utils/database.php");
	$username = $_SESSION["username"];
	$dbh = new DatabaseHelper("localhost", "root", "", "shareit", 3306);
?>