<!DOCTYPE html>

<html lang="it">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ShareIT - Login</title>
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
</head>
<body>
    <header>
        <h1 class="m-4 text-light">ShareIT</h1>
    </header>
	
	<?php if(isset(($templateParams["login_error"]))) : ?>
	<div class="alert alert-danger mt-5 ml-5 mr-5"><?php echo $templateParams["login_error"] ?></div>
	<?php endif; ?>
	
	<form method="POST" action="/login.php">
		<div class="form-group bg-white m-5 p-5">
			<h3>Login</h3>
			
			<label for="username" class="mt-2">Username</label>
			<input type="text" class="form-control" name="username">
			
			<label for="password" class="mt-2">Password</label>
			<input type="password" class="form-control" name="password">
			
			<input type="submit" class="btn btn-primary mt-3" value="Login">
		</div>
	</form>
	
    <footer>
        <p class="text-info m-3">Riccardo Fragozzi - A.A. 2022/2023</p>
    </footer>
</body>
</html>