<div class="modal fade mt-5" id="usersModal" tabindex="-1" role="dialog" aria-labelledby="usersModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Trova utenti</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Chiudi">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<input id="users-input" type="text" class="roundinput">
		<div id="users-result" class="flex">
		</div>
      </div>
    </div>
  </div>
</div>
<script>
   $("#usersModal").on("input", function(e) {
	   usersSearch();
   });
</script>