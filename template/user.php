<!DOCTYPE html>

<html lang="it">
<head>
	<?php require("template/includes.php") ?>
</head>
<body>
	<header class="d-flex inline-flex align-items-center p-2">
		<?php require("template/header.php") ?>
	</header>
	<?php
		
		$user = $templateParams["user"];
		$myself = getUsername() == $user["username"];
		$followed = in_array(getUsername(), array_map(function($user) { return $user["username"]; }, $user["followers"]));
		
		$templateParams["userlistmodal-title"] = "Follower di " . $user["name"];
		$templateParams["userlistmodal-id"] = "userlist-follower-modal";
		$templateParams["userlistmodal-users"] = $user["followers"];
		require("template/userlist-modal.php");
		
		$templateParams["userlistmodal-title"] = "Persone che segue " . $user["name"];
		$templateParams["userlistmodal-id"] = "userlist-followed-modal";
		$templateParams["userlistmodal-users"] = $user["followed"];
		require("template/userlist-modal.php");
		
	?>

	<main class="d-flex align-items-center flex-column">
		<img src="css/arrow-left-solid.svg" width="25" onClick="home()" class="mr-auto mt-4 ml-4" alt="back" aria-label="back-button"/>
		<img class="rounded-circle" width="160" height="160" src="<?php echo $user["avatar"] ?>" alt="avatar di <?php echo $user["username"] ?>">
		<h3 class="mt-2 mb-0"><?php echo $user["name"] . " " . $user["surname"] ?></h3>
		<p class="mt-1">@<?php echo $user["username"] ?></p>
		
		<div class="row inline-flex justify-content-center">
			<div class="d-flex flex-column bg-light m-1 p-2 rounded" data-toggle="modal" data-target="#userlist-follower-modal" style="width:100px">
				<h5 class="m-auto"><?php echo count($user["followers"]) ?></h5>
				<p class="text-secondary text-small m-auto">follower</p>
			</div>
			<div class="d-flex flex-column bg-light m-1 p-2 rounded" data-toggle="modal" data-target="#userlist-followed-modal" style="width:100px">
				<h5 class="m-auto"><?php echo count($user["followed"]) ?></h5>
				<p class="text-secondary text-small m-auto">seguiti</p>
			</div>
		</div>
		
		<?php if(!$myself) : ?>
		<button id="follow-<?php echo $user["username"] ?>" type="button" class="btn btn-secondary mt-2" val="<?php echo $followed ? "1" : "0" ?>" aria-label="<?php echo $followed ? "Non seguire" : "Segui" ?>" onClick="followUser('<?php echo $user["username"] ?>')"><?php echo $followed ? "Non seguire" : "Segui" ?></button>
		<?php endif; ?>		
		<?php
		if(count($user["posts"]) == 0) : ?>
			<p class="lead mt-5">Ancora nessun post</p>
		<?php endif; ?>
		<div class="card-columns m-3">
			<?php 
			foreach($user["posts"] as $post){
				$templateParams["post"] = $post;
				require("template/post.php");
			}
			 ?>
		</div>
    </main>
    <footer>
        <p>Tecnologie Web - A.A. 2022/2023</p>
    </footer>
</body>
</html>