<div class="modal fade mt-5" id="<?php echo $templateParams["userlistmodal-id"] ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $templateParams["userlistmodal-id"] ?>" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<h5><?php echo $templateParams["userlistmodal-title"] ?></h5>
				<div id="users-result" class="flex">
					<?php foreach($templateParams["userlistmodal-users"] as $userlistmodal_user): 
					$userlistmodal_user_avatar = $userlistmodal_user["avatar"];
					$userlistmodal_user_name = $userlistmodal_user["name"];
					$userlistmodal_user_id = $userlistmodal_user["username"];
					?>
					<div class="mt-2">
						<img class="rounded-circle" width="20" height="20" src="<?php echo $userlistmodal_user_avatar ?>" alt="avatar di <?php echo $userlistmodal_user_name ?>">
						<a href="user.php?id=<?php echo $userlistmodal_user_id ?>"><?php echo $userlistmodal_user_name ?></a>
					</div>
					<?php endforeach; ?>
					<?php if(count($templateParams["userlistmodal-users"]) == 0): ?>
					<p class="m-auto text-secondary">Nessun utente</p><?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>