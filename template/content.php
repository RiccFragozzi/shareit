<!DOCTYPE html>
<?php	
	$post = $templateParams["post"];
	if(sizeof($post) == 0){
		header("HTTP/1.0 404 Not Found");
		die();
	}	
?>

<html lang="it">
<head>
	<?php require("template/includes.php") ?>
</head>
<body>
	<header class="d-flex inline-flex align-items-center p-2">
		<?php require("template/header.php") ?>
	</header>
	<main class="d-flex align-items-center flex-column pt-5">
		<?php
			require("template/post.php");
		?>
    </main>
    <footer>
        <p>Tecnologie Web - A.A. 2022/2023</p>
    </footer>
</body>
</html>