<header class="d-flex inline-flex align-items-center p-2">
	<img src="css/logo-header.svg" height="30" class="ml-2" onClick="home()" alt="home" aria-label="home-button"/>
	<img id="btn-search" src="css/search-solid.svg" height="20" class="ml-auto mr-2" data-toggle="modal" data-target="#usersModal" alt="search" aria-label="search-user-button"/>
	<img id="btn-users" src="css/user-solid.svg" height="20" class="ml-2 mr-2" onClick="profile()" alt="profile" aria-label="prifile-button"/>
	<img id="btn-bell" src="css/bell-solid.svg" height="20" class="ml-2 mr-2" data-toggle="modal" data-target="#notificationsModal" alt="notifications" aria-label="notifications-button"/>
	<img src="css/logout.svg" height="20" class="ml-2 mr-2" onClick="logout()" alt="logout" aria-label="logout-button"/>
	<div id="notification-count" class="notification-count"></div>
	<?php require("template/users-modal.php") ?>
	<?php require("template/notifications-modal.php") ?>
</header>