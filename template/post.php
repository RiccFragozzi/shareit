<?php 
	$post = $templateParams["post"];
	$postid = $post["id"];
	$liked = in_array(getUsername(), $post["likes"]);
	$postalt = "Post di " . $post["author_name"] . ". " . (strlen($post["description"]) == 0 ? "Nessuna descrizione." : $post["description"]);
?>

<article class="card post">
	<div class="d-flex inline-flex align-items-center m-1 ml-2">
		<img class="rounded-circle" width="30" height="30" src="<?php echo $post["author_avatar"] ?>" alt="avatar di <?php echo $post["author_name"] ?>">
		<a class="card-subtitle m-2 text-muted" href="/user.php?id=<?php echo $post["author_id"] ?>"><?php echo $post["author_name"] ?></a>
		<img id="like-<?php echo $post["id"] ?>" src="css/<?php echo $liked ? "heart-solid.svg" : "heart-regular.svg" ?>" height="20" class="ml-auto mr-2" val=<?php echo $liked ? "1" : "0" ?> onClick="likePost('<?php echo $post["id"] ?>')" alt="like" aria-label="like-button"/>
	</div>
	<?php if(strlen($post["media"])>0) : ?>
		<img class="card-img-top" src="<?php echo $post["media"] ?>" alt="<?php echo $postalt ?>">
	<?php endif; ?>		
	<div class="card-body pt-1">
		<p class="card-text font-weight-light font-italic mt-0 mb-0"><?php echo $post["description"] ?></p>
		<?php foreach($post["comments"] as $comment): ?>
			<p class="font-weight-light text-muted small m-0">
				<a href="user.php?id=<?php echo $comment["user_id"] ?>" class="font-weight-bold link-secondary"><?php echo $comment["user_id"] ?></a>
				<font class="font-italic"><?php echo $comment["content"] ?></font>
			</p>
		<?php endforeach; ?>
		
		<div class="d-flex inline-flex align-items-center mt-2 p-0">
			<input id="comment-<?php echo $postid ?>" type="text" placeholder="Inserisci un commento" aria-label="Inserisci un commento" class="roundinput m-0">
			<img src="css/share-solid.svg" height="20" class="ml-2" onClick="commentConfirm(<?php echo $postid ?>)" alt="comment" aria-label="comment-button"/>
		</div>
		
	</div>
</article>