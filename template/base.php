<!DOCTYPE html>
<html lang="it">
<head>
	<?php require("template/includes.php") ?>
	<script>
	var upload_image_url = "";
	
	function postImageUpload(){
		var file = document.getElementById("post-image-selector")['files'][0];
		var reader = new FileReader();
		var baseString;
		reader.onloadend = function () {
			baseString = reader.result.substr(reader.result.indexOf(',') +1, reader.result.length);
			var fd = new FormData();
			fd.append('imageData', baseString);
			fd.append('extension', '.png');

			$.ajax({
				url: 'api/image-upload.php',
				type: 'post',
				data: fd,
				contentType: false,
				processData: false,
				success: function(response){
					upload_image_url = response;
					$("#post-image").attr("src", upload_image_url);
					$("#post-image").removeClass("d-none");
				},
			});
		};
		reader.readAsDataURL(file);
	}
	
	function postConfirm(){
		var description = $("#post-description").val();
		if(description.length == 0 && upload_image_url.length == 0){
			alert("Carica un'immagine o scrivi un commento per iniziare un post");
			return;
		}
		createPost(description, upload_image_url);
		upload_image_url = "";
	}
	</script>
</head>
<body>
	<?php require("template/header.php") ?>
    <nav>
        <h5 class="display-5">Ciao, <?php echo $templateParams["name"] ?></h5>
		<img id="post-image" class="w-100 border rounded rounded-lg d-none" alt="post-preview">
		<div class="d-flex inline-flex align-items-center mt-3">
			<input id="post-description"type="text" placeholder="Scrivi cosa pensi" class="roundinput">
			<input id="post-image-selector" type="file" accept="image/png, image/gif, image/jpeg" style="display:none" onChange="postImageUpload()">
			<img src="css/image-regular.svg" height="20" class="mr-2 ml-2" onClick="$('#post-image-selector').click()" alt="add-image-button" aria-label="add-image-button"/>
			<img src="css/share-solid.svg" height="20" class="ml-2" onClick="postConfirm()" alt="send-post-button" aria-label="send-post-button"/>
		</div>
    </nav>
	<main class="container p-2 mt-3">
		<div class="card-columns">
			<?php 
			foreach($templateParams["feed"] as $post){
				$templateParams["post"] = $post;
				require("template/post.php");
			}
			?>
		</div>
    </main>
    <footer>
        <p>Tecnologie Web - A.A. 2022/2023</p>
    </footer>
</body>
</html>