<div class="modal fade mt-5" id="notificationsModal" tabindex="-1" role="dialog" aria-labelledby="notificationsModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
	  
        <button type="button" class="close" data-dismiss="modal" aria-label="Chiudi">
          <span aria-hidden="true">&times;</span>
        </button>
		
		<div id="notifications-list" class="flex mt-5">
		</div>
      </div>
    </div>
  </div>
</div>

<script>
function updateNotifications(notifications){	
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if(this.readyState == 4 && this.status == 200){
			var response = JSON.parse(this.response);
			if(response.code != 1){
				return;
			}
			
			$("#notifications-list").html("");
			
			$("#notification-count").css("display", response.output.length > 0 ? "block" : "none");
			$("#notification-count").html(response.output.length);
			
			for(var i in response.output){
				var notification = response.output[i];
				var notification_id = 'notification-' + notification.id;
				var notification_user_href = "/user.php?id=" + notification.source_user;
				var notification_icon = "";
				var notification_content = "";
				
				if(notification.type == 1){
					notification_icon = "css/follow.svg";
					notification_content = "ha iniziato a seguirti";
				}else if(notification.type == 2){
					notification_icon = "css/like.svg";
					notification_content = "ha messo like al tuo post";
				}else if(notification.type == 3){
					notification_icon = "css/comment.svg";
					notification_content = "ha commentato il tuo post";
				}else{
					continue;
				}
				
				jQuery('<div>', {
					id: notification_id,
					class: 'd-flex inline-flex align-items-center m-1 ml-2 mt-1 p-1 bg-light border rounded',
				}).appendTo('#notifications-list');
				
				jQuery('<img>', {
					class: 'rounded-circle mr-2',
					width: 30,
					height: 30,
					src: notification_icon,
				}).appendTo('#' + notification_id);
				
				$('#' + notification_id).append('<p class="mt-auto mb-auto"><a href="' + notification_user_href + '">' + notification.source_user + '</a> ' + notification_content + '</p>');
				
				jQuery('<a>', {
					class: 'btn btn-sm btn-dark ml-2 text-light',
					href: notification.href,
					text: "Mostra",
				}).appendTo('#' + notification_id);			
			}
		}
	};
	
	xmlhttp.open("GET", "api/get-notifications.php", true);
	xmlhttp.send();
}

setInterval(updateNotifications, 1000);


</script>