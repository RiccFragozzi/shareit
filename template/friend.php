	<?php $friend = $templateParams["friend"]; ?>
	<?php $followed = $friend["followed"]; ?>
	
	<div class="d-flex inline-flex align-items-center m-1 ml-2">
		<img class="rounded-circle" width="30" height="30" src="<?php echo $friend["avatar"] ?>" alt="avatar di <?php echo $friend["name"] ?>">
		<a class="card-subtitle m-2 text-muted" href="/user.php?id=<?php echo $friend["id"] ?>"><?php echo $friend["name"] ?></a>
		<a id="follow-<?php echo $friend["id"] ?>" class="ml-auto mr-2" onClick="followUser('<?php echo $friend["id"] ?>')"><?php echo $followed ? "Rimuovi" : "Segui" ?></a>
	</div>