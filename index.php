<?php
require_once 'utils/bootstrap.php';
 
//Base Template
$templateParams["titolo"] = "Blog TW - Home";
$templateParams["nome"] = "lista-articoli.php";

if(!isUserLoggedIn()){
	header('Location: /login.php');
	exit();
}

$templateParams["feed"] = $dbh->getFeed(getUsername());
$templateParams["username"] = getUsername();
$templateParams["name"] = getName();

require 'template/base.php';
?>