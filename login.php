<?php
require_once 'utils/bootstrap.php';
require("template/includes.php");

if(isset($_POST["username"]) && isset($_POST["password"])){
    $login_result = $dbh->checkLogin($_POST["username"], $_POST["password"]);
    if(strlen($login_result)==0){
        //Login fallito
        $templateParams["login_error"] = "Errore! Controllare username o password!";
    }
    else{
        registerLoggedUser($_POST["username"], $login_result);
    }
}

if(isUserLoggedIn()){
    $templateParams["titolo"] = "ShareIt";
	header('Location: /index.php');
}
else{
	require 'template/login.php';
}

?>