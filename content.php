<?php
require_once 'utils/bootstrap.php';

if(!isUserLoggedIn()){
	header('Location: /login.php');
	exit();
}

if(isset($_GET["id"]) && strlen($_GET["id"]) > 0){
	$post = $_GET["id"];
}else{
	header("HTTP/1.0 404 Not Found");
	die();
}

$templateParams["post"] = $dbh->getPost($post);

require 'template/content.php';
?>