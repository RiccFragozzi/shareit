<?php
class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname, $port){
        $this->db = new mysqli($servername, $username, $password, $dbname, $port);
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }        
    }

    public function getFeed($user_id){
		$feed = array();
        $stmt = $this->db->prepare("SELECT id FROM posts WHERE user_id IN (SELECT user2_id FROM friends WHERE user1_id=?) ORDER BY timestamp DESC");
        $stmt->bind_param('s', $user_id);
        $stmt->execute();
        $result = $stmt->get_result();
		while($row = $result->fetch_assoc()){
			array_push($feed, $this->getPost($row["id"]));
		}
		
        return $feed;
    }

    public function getFollowers($user_id){
		$followers = array();
        $stmt = $this->db->prepare("SELECT username, name, surname, avatar FROM friends, users WHERE user2_id=? and username = user1_id");
        $stmt->bind_param('s', $user_id);
        $stmt->execute();
        $result = $stmt->get_result();
		while($row = $result->fetch_assoc()){			
			$user = array(
				'username' => $row["username"],
				'name' => $row["name"],
				'surname' => $row["surname"],
				'avatar' => $row["avatar"]
			);			
			array_push($followers, $user);
		}
		
        return $followers;
    }

    public function getFollowed($user_id){
		$followed = array();
        $stmt = $this->db->prepare("SELECT username, name, surname, avatar FROM friends, users WHERE user1_id=? and username = user2_id");
        $stmt->bind_param('s', $user_id);
        $stmt->execute();
        $result = $stmt->get_result();
		while($row = $result->fetch_assoc()){			
			$user = array(
				'username' => $row["username"],
				'name' => $row["name"],
				'surname' => $row["surname"],
				'avatar' => $row["avatar"]
			);			
			array_push($followed, $user);
		}
		
        return $followed;
    }

    public function getPosts($user_id){
		$posts = array();
        $stmt = $this->db->prepare("SELECT id, media, description, timestamp FROM posts WHERE user_id=? ORDER BY timestamp DESC");
        $stmt->bind_param('s', $user_id);
        $stmt->execute();
        $result = $stmt->get_result();
		while($row = $result->fetch_assoc()){
			array_push($posts, $this->getPost($row["id"]));
		}
		
        return $posts;
    }

    public function getPost($post_id){
		$query = "
			SELECT 
				id,
				user_id AS author_id,
				users.avatar AS author_avatar,
				users.name AS author_name,
				media,
				description,
				timestamp
			FROM posts, users
			WHERE id=? AND posts.user_id = users.username 
			ORDER BY timestamp
		";
		$post = array();
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $post_id);
        $stmt->execute();
        $result = $stmt->get_result();
		while($row = $result->fetch_assoc()){
			$post = [
				'id' => $row["id"],
				'author_id' => $row["author_id"],
				'author_name' => $row["author_name"],
				'author_avatar' => $row["author_avatar"],
				'media' => $row["media"],
				'description' => $row["description"],
				'timestamp' => $row["timestamp"],
				'likes'=>$this->getLikes($row["id"]),
				'comments'=>$this->getComments($row["id"]),
			];
		}
		
        return $post;
    }

    public function getLikes($post_id){
		$likes = array();
        $stmt = $this->db->prepare("SELECT user_id FROM likes WHERE post_id=?");
        $stmt->bind_param('i', $post_id);
        $stmt->execute();
        $result = $stmt->get_result();
		while($row = $result->fetch_assoc()){
			array_push($likes, $row["user_id"]);
		}
		
        return $likes;
    }

    public function getComments($post_id){
		$comments = array();
        $stmt = $this->db->prepare("SELECT id, content, user_id, name FROM comments, users WHERE post_id=? AND user_id = users.username ORDER BY id DESC");
        $stmt->bind_param('i', $post_id);
        $stmt->execute();
        $result = $stmt->get_result();
		while($row = $result->fetch_assoc()) {
			$comment = [
				'id' => $row["id"],
				'content' => $row["content"],
				'user_id' => $row["user_id"],
				'user_name' => $row["name"]
			];
			array_push($comments, $comment);
		}
		
        return $comments;
    }
	
	public function createPost($username, $description, $media){
        $stmt = $this->db->prepare("INSERT INTO posts(media, description, timestamp, user_id) VALUES(?, ?, UNIX_TIMESTAMP(), ?)");
        $stmt->bind_param('sss', $media, $description, $username);
        return $stmt->execute();
	}
	
	public function likePost($username, $post_id){
        $stmt = $this->db->prepare("INSERT INTO likes(post_id, user_id) VALUES(?, ?)");
        $stmt->bind_param('is', $post_id, $username);
        $result = $stmt->execute();
		$this->createNotification($this->getUserByPost($post_id), 2, "/content.php?id=$post_id", $username);
		return $result;
	}
	
	public function dislikePost($username, $post_id){
        $stmt = $this->db->prepare("DELETE FROM likes WHERE post_id=? AND user_id=?");
        $stmt->bind_param('is', $post_id, $username);
        $result = $stmt->execute();
		return $result;
	}
	
	public function commentPost($username, $post_id, $content){
        $stmt = $this->db->prepare("INSERT INTO comments(content, user_id, post_id) VALUES(?, ?, ?)");
        $stmt->bind_param('ssi', $content, $username, $post_id);
        $result = $stmt->execute();
		$this->createNotification($this->getUserByPost($post_id), 3, "/content.php?id=$post_id", $username);
		return $result;
	}
	
	public function followUser($username, $user_id){
        $stmt = $this->db->prepare("INSERT INTO friends(user1_id, user2_id) VALUES(?, ?)");
        $stmt->bind_param('ss', $username, $user_id);
        $result = $stmt->execute();
		$this->createNotification($user_id, 1, "/user.php?id=$username", $username);
		return $result;
	}
	
	public function unfollowUser($username, $user_id){
        $stmt = $this->db->prepare("DELETE FROM friends WHERE user1_id=? AND user2_id=?");
        $stmt->bind_param('ss', $username, $user_id);
        return $stmt->execute();
	}

    public function getUser($user_id){
        $stmt = $this->db->prepare("SELECT avatar, email, name, surname FROM users WHERE username=?");
        $stmt->bind_param('s', $user_id);
        $stmt->execute();
        $result = $stmt->get_result();
		while($row = $result->fetch_assoc()) {
			return [
				'username' => $user_id,
				'avatar' => $row["avatar"],
				'email' => $row["email"],
				'name' => $row["name"],
				'surname' => $row["surname"],
				'followers' => $this->getFollowers($user_id),
				'followed' => $this->getFollowed($user_id),
				'posts' => $this->getPosts($user_id)
			];
		}
		
        return array();
    }

    public function getUserByPost($post_id){
        $stmt = $this->db->prepare("SELECT user_id FROM posts WHERE id=?");
        $stmt->bind_param('i', $post_id);
        $stmt->execute();
        $result = $stmt->get_result();
		while($row = $result->fetch_assoc()) {
			return $row["user_id"];
		}
		
        return -1;
    }

    public function getNotifications($user_id){
        $stmt = $this->db->prepare("SELECT id, type, source_user, href FROM notifications WHERE user_id = ? ORDER BY id DESC");
        $stmt->bind_param('s', $user_id);
        $stmt->execute();
        $result = $stmt->get_result();
		$notifications = array();
		while($row = $result->fetch_assoc()) {
			array_push($notifications, [
				'id' => intval($row["id"]),
				'username' => $user_id,
				'type' => intval($row["type"]),
				'source_user' => $row["source_user"],
				'href' => $row["href"]
			]);
		}
		
        return $notifications;
    }

    public function removeNotification($notification_id){
        $stmt = $this->db->prepare("DELETE FROM notifications WHERE id = ?");
        $stmt->bind_param('s', $user_id);
        $stmt->execute();
        $result = $stmt->get_result();
		return $result;
    }
	
	public function createNotification($user_id, $type, $href, $source_user){
        $stmt = $this->db->prepare("INSERT INTO notifications(type, href, source_user, user_id) VALUES(?, ?, ?, ?)");
        $stmt->bind_param('isss', $type, $href, $source_user, $user_id);
        return $stmt->execute();
	}

    public function checkLogin($username, $password){
		$password = md5($password);
        $query = "SELECT name FROM users WHERE username = ? AND password = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();

		if($result->num_rows == 0){
			return "";
		}
        return $result->fetch_assoc()["name"];
    }

    public function searchUser($query){
		$users = array();
        $stmt = $this->db->prepare("SELECT username, avatar, name, surname FROM users WHERE username=? OR CONCAT(name, ' ', surname) LIKE CONCAT('%', ?, '%')");
        $stmt->bind_param('ss', $query, $query);
        $stmt->execute();
        $result = $stmt->get_result();
		while($row = $result->fetch_assoc()) {
			$user = [
				'username' => $row["username"],
				'avatar' => $row["avatar"],
				'name' => $row["name"],
				'surname' => $row["surname"],
				//'followed' => $this->getFollowed($user_id),
			];
			array_push($users, $user);
		}
		
        return $users;
    }

}
?>