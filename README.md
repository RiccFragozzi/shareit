# shareit
Copy all content of repository (except for shareit.sql) into your FTP space.
Configure your database credentials into api/api.php file.
Import MySQL structure from shareit.sql file.
You can login with following users:
- chiaraferragni (pwd: chiara)
- fedez (pwd: fedez)
- checcozalone (pwd: checco)